const nodemailer = require('nodemailer');
const nodemailerSendgrid = require('nodemailer-sendgrid');

const transporter = nodemailer.createTransport(
    nodemailerSendgrid({
        apiKey: 'jdbfkjbjkbfjgbjkfbgggg^R&^%RFUBKUYGOUIBILBJK'
    })
);
const sendMail = exports.sendMail = function (details) {
    console.log("company details",details)
    const mailOptions = {
        to: details.to.toLowerCase(),
        subject: "Enquiry at Brandhawker",
        // attachments: [{
        //     filename: 'mailLogo.png',
        //     filename:'bg.jpg',
        //     path: '/home/omkar/Downloads/New Design Payflow/vendormsbackend-payflow-local/public/mailLogo.png',
        //     cid: 'unique@cid',
        // }],
        html:  '<div style="border:1px solid #D4D4D4 ;padding:20px">' +
            '<div style="height:60;width:190;left:50%"><img height="100" width="130" src="cid:unique@cid" alt="PayFlow"/></div>'+
         `<strong style="color:orange">Welcome &nbsp;&nbsp; ${details.name}, </strong>` +
          '<p>Your application has been submitted in Brandhawkers .</p>'+
            '<p>We will get back to you soon!</p>'+
            '</div>'+
        '</div>' ,
        cc: details.cc || null,
        from: details.from || process.env.DEFAULT_EMAIL_FROM_ADDRESS
    };

    return transporter.sendMail(mailOptions)
        .then(([res]) => {
            console.log('Message delivered with code %s %s', res.statusCode, res.statusMessage);
            return res;
        })
        .catch(err => {
            console.log('Errors occurred, failed to deliver message');

            if (err.response && err.response.body && err.response.body.errors) {
                err.response.body.errors.forEach(error => console.log('%s: %s', error.field, error.message));
            } else {
                console.log(err);
            }
            return false;
        });
};

