import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';


import img1 from '../../images/associates/apple.jpeg';
import img2 from '../../images/clients/2.png';
import img3 from '../../images/clients/3.png';
import img4 from '../../images/clients/client1.jpg';
import img5 from '../../images/clients/Assetsoft.jpg';
import img6 from '../../images/clients/congizant.png';
import img7 from '../../images/clients/Vaarivana.png';
import img8 from '../../images/clients/parandeSpaces.jpg';
import img9 from '../../images/clients/sahyadri.png';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import SliderParent from "./sliderParent"; // requires a loader
var Carousel = require('react-responsive-carousel').Carousel;

class Associates extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {

        return (
            <React.Fragment>
                <section style={{textAlign:'center' }} id={"clients"} className="section-sm bg-light">
                    <div className="container">
                        <Row style={{textAlign:'center' ,paddingBottom:60}}>
                            <Col lg="8" className="offset-lg-2">
                                <h1 className="section-title text-center">Our Associates</h1>
                            </Col>
                        </Row>

                        <SliderParent/>





                    </div>
                </section>
            </React.Fragment >
        );
    }
}

export default Associates;


