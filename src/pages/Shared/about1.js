
import React, { Component } from 'react';

import img1 from '../../images/team/img-5.jpg';
import img2 from '../../images/team/img-6-min.jpg';
import img3 from '../../images/team/img-3.jpg';
import img4 from '../../images/team/img-4.jpg';

class About1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        
        return (
            <React.Fragment>
                 <section className="section" id="about">
            <div className="container">
                <div className="row">
                    <div className="col-lg-8 offset-lg-2">
                        <div className="about-title mx-auto text-center">
                            <h2 className="font-weight-light">With us comes our team, as an extension. Whether it is our core team, people would
                                influence us or you, or a photographer, or web developer, or another business that could
                                help, the list goes on.... </h2>
                            <p className="text-muted pt-4">A duo of the ‘young and the restless’, is what we call ourselves. Young. Spirited. We believe
                                in staying hungry, not foolish, with a ‘never say never’ attitude. Of course, we never forget
                                who we are and where we came from. More like a modern touch to a traditional us. We make
                                sure we know where we’re headed. Just like you!</p>
                        </div>
                    </div>
                </div>
                <div className="row margin-t-50">
                    <div className="col-lg-6 col-sm-12">
                        <div className="team-box text-center">
                            <div className="team-wrapper">
                                <div className="team-member">
                                    <img alt="" src={img1} style={{width:'300px' ,height:'300px'}}  className="img-fluid mx-auto d-block img-thumbnail rounded-circle mb-4" />
                                </div>
                            </div>
                            <h4 className="team-name">Aditya Deshmukh</h4>
                            <p className="text-uppercase team-designation">CEO</p>
                        </div>
                    </div>

                    <div className="col-lg-6 col-sm-12">
                        <div className="team-box text-center">
                            <div className="team-wrapper">
                                <div className="team-member">
                                    <img alt="" src={img2} style={{width:'300px' }} className="img-fluid mx-auto d-block img-thumbnail rounded-circle mb-4" />
                                </div>
                            </div>
                            <h4 className="team-name">Mayur Bhojane</h4>
                            <p className="text-uppercase team-designation">Founder</p>
                        </div>
                    </div>

                    {/*<div className="col-lg-3 col-sm-6">*/}
                    {/*    <div className="team-box text-center">*/}
                    {/*        <div className="team-wrapper">*/}
                    {/*            <div className="team-member">*/}
                    {/*                <img alt="" src={img3} className="img-fluid rounded" />*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*        <h4 className="team-name">Wanda Arthur</h4>*/}
                    {/*        <p className="text-uppercase team-designation">Developer</p>*/}
                    {/*    </div>*/}
                    {/*</div>*/}

                    {/*<div className="col-lg-3 col-sm-6">*/}
                    {/*    <div className="team-box text-center">*/}
                    {/*        <div className="team-wrapper">*/}
                    {/*            <div className="team-member">*/}
                    {/*                <img alt="" src={img4} className="img-fluid rounded" />*/}
                    {/*            </div>*/}
                    {/*        </div>*/}
                    {/*        <h4 className="team-name">Joshua Stemple</h4>*/}
                    {/*        <p className="text-uppercase team-designation">Manager</p>*/}
                    {/*    </div>*/}
                    {/*</div>*/}

                </div>
            </div>
        </section>
              
            </React.Fragment >
        );
    }
}

export default About1;


