
import React, { Component } from 'react';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import {Button} from "reactstrap";
// const mailHelper = require('../../helpers/mailHelper');

class ContactUs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name:null,
            email:null,
            subject:null,
            message:null
        }
    }
    // sendMail=()=>{
    //     console.log(this.state)
    //     const mail = mailHelper.sendMail({
    //         to: this.state.email,
    //         cc: ['abhay@nativebyte.in'],
    //         name:this.state.name,
    //     })
    // }
    render() {

        return (
            <React.Fragment>
                <section className="section " id="contact">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 offset-lg-2">
                                <h1 className="section-title text-center">Get In Touch</h1>
                                <div className="section-title-border margin-t-20"></div>
                                <p className="section-subtitle text-muted text-center font-secondary padding-t-30">We thrive when coming up with innovative ideas but also understand that a smart concept should be supported with measurable results.</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-4">
                                <div className="mt-4 pt-4">
                                    <p className="mt-4"><span className="h5">Office Address:</span><br /> <span className="text-muted d-block mt-2">OSCAR MEDIA AND ENTERTAINMENT <br /> Kothrud , Pune 4110 38</span></p>
                                    <p className="mt-4"><span className="h5">Email         :</span><br /> <span className="text-muted d-block mt-2">email@brandhawker.in</span></p>
                                    <p className="mt-4"><span className="h5">Phone         :</span><br /> <span className="text-muted d-block mt-2">8698961928</span></p>
                                    <p className="mt-4"><span className="h5">Working Hours:</span><br /> <span className="text-muted d-block mt-2">9:00AM To 6:00PM</span></p>

                                </div>
                            </div>
                            <div className="col-lg-8">
                                <div className="custom-form mt-4 pt-4">
                                    <div id="message"></div>
                                    <AvForm id="contact-form">
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <div className="form-group mt-2">
                                                   <input type="text" name="name" className="form-control" onChange={(x)=>this.setState({
                                                       name:x.target.value
                                                   })} placeholder="Your name*"  required />
                                                </div>
                                            </div>
                                            <div className="col-lg-6">
                                                <div className="form-group mt-2">
                                                <input type="email" name="email" className="form-control" onChange={(x)=>this.setState({
                                                    email:x.target.value
                                                })} placeholder="Your email*"  required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mt-2">
                                                <input type="text"   name="subject" className="form-control" onChange={(x)=>this.setState({
                                                    subject:x.target.value
                                                })} placeholder="Your Subject.."  required />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className="form-group mt-2">
                                                    <input name="comments"  id="comments" rows="4" className="form-control" onChange={(x)=>this.setState({
                                                        message:x.target.value
                                                    })} placeholder="Your message..."/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-lg-12 text-right">
                                                <Button  id="submit"  style={{width:100,height:40}} name="send" className="submitBnt btn btn-custom" value="Send Message" >
                                                    Notify Mee
                                                </Button>
                                                <div id="simple-msg"></div>
                                            </div>
                                        </div>
                                    </AvForm>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </React.Fragment>
        );
    }
}

export default ContactUs;


