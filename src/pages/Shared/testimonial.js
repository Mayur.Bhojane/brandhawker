
import React, { Component } from 'react';

import user1 from '../../images/testimonials/Design.jpg';
import user2 from '../../images/testimonials/Shubha.jpg';
import user3 from '../../images/testimonials/Fiesta.jpg';

class Testimonial extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {

        return (
            <React.Fragment>
                <section className="section" id="testi">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-8 offset-lg-2">
                                <h1 className="section-title text-center">What they've said</h1>
                                <div className="section-title-border margin-t-20"></div>
                                <p className="section-subtitle text-muted text-center font-secondary padding-t-30">If your partner has a blog, write guest posts
                                    Bloggers often guest post on other websites to get more exposure and increase their
                                    website traffic. This type of partnership is a good deal for both the parties as one party
                                    gets quality content while other get more exposure and a backlink to his website.</p>
                            </div>
                        </div>
                        <div className="row margin-t-50">
                            <div className="col-lg-4">
                                <div className="testimonial-box margin-t-30">
                                    <div className="testimonial-decs p-4">
                                        <div className="testi-icon">
                                            <i className="mdi mdi-format-quote-open display-2"></i>
                                        </div>
                                        <img src={user1} alt="" className="img-fluid mx-auto d-block img-thumbnail rounded-circle mb-4" />
                                        <div className="p-1">
                                            <h5 className="text-center text-uppercase mb-3">Khan Shaibaaz - <span className="text-muted text-capitalize">Design Tantra</span></h5>
                                            <p className="text-muted text-center mb-0">“Their team is knowledgeable, responsive and committed to supporting our initiatives, making them invaluable partners in our effort to promote brand awareness, drive traffic to our website, and generate new business. We feel confident that we’re getting the best advice on how to effectuate our digital marketing strategy.” </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="testimonial-box margin-t-30">
                                    <div className="testimonial-decs p-4">
                                        <div className="testi-icon">
                                            <i className="mdi mdi-format-quote-open display-2"></i>
                                        </div>
                                        <img src={user2} alt="" className="img-fluid mx-auto d-block img-thumbnail rounded-circle mb-4" />
                                        <div className="p-1">
                                            {/*asdsad        szaacasc*/}
                                            <h5 className="text-center text-uppercase mb-3">Deepak Ghate - <span className="text-muted text-capitalize">Shubharambh Events</span></h5>
                                            <p className="text-muted text-center mb-0">For several years Shubharambh Events has been working with Brand Hawker on marketing campaigns. As well, they have also developed two websites for us. We were very pleased with all the work and expertise provided." </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-4">
                                <div className="testimonial-box margin-t-30">
                                    <div className="testimonial-decs p-4">
                                        <div className="testi-icon">
                                            <i className="mdi mdi-format-quote-open display-2"></i>
                                        </div>
                                        <img src={user3} alt="" className="img-fluid mx-auto d-block img-thumbnail rounded-circle mb-4" />
                                        <div className="p-1">
                                            <h5 className="text-center text-uppercase mb-3">Varad Karve - <span className="text-muted text-capitalize">Fiesta Media and Entertainment</span></h5>
                                            <p className="text-muted text-center mb-0">“Brand Hawker is a remarkable  Marketing Agency. Aditya Deshmukh and his team are top notch professionals and among the hardest working, most creative and innovative people - They know their stuff, inside and out and have consistently delivered amazing results to us over the past 5 years. I highly recommend their services.” </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment >
        );
    }
}

export default Testimonial;


