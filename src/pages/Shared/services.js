
import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';


class Services extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {

        return (
            <React.Fragment>
                <section className="section bg-light" id="services">
                    <div className="container">
                        <Row>
                            <Col lg="8" className="offset-lg-2">
                                <h1 className="section-title text-center">Our Services</h1>
                                <div className="section-title-border margin-t-20"></div>
                                <p className="section-subtitle text-muted text-center padding-t-30 font-secondary">We make sure that from the very beginning you have a consistent team,
                                    a team who knows your brand inside and out and cares about it as much
                                    as you do. Making the complicated sound perfectly simple, is the
                                    essence of technology. In addition, creative communication marks your
                                    presence through audience retention, engaging conversations &
                                    comparative analysis and that's WHAT WE DO BEST.</p>
                            </Col>
                        </Row>
                        <div className="row margin-t-30">
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-diamond text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Offer Based Cross Promotion</h4>
                                            <p className="pt-2 text-muted">In this use case, two or more brands cross-promote each other’s offers. This is a great way to build
                                                customer loyalty (by gratifying them with an offer) while at the same time generating traffic from
                                                the other brand – an amazing win-win situation.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-display2 text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Contest Based Cross Promotion</h4>
                                            <p className="pt-2 text-muted">This particular use case works well if one of the brands can offer sponsoring a membership or
                                                product for free for lucky draw contest winners.</p>

                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-piggy text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Brand Awareness Cross Promotion</h4>
                                            <p className="pt-2 text-muted">Cross promotion can be an incredible way to create awareness for your brand.
                                                A great example is the cross promotion between Uber and Spotify. Uber allowed riders to choose
                                                their own playlist that’ll stream as they ride to their destination.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </div>
                        <Row>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-science text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Content Based Cross Promotion</h4>
                                            <p className="pt-2 text-muted">Content based cross promotion campaigns involve both brands working together to produce
                                                content for their users.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-news-paper text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Make use of each other’s assets.</h4>
                                            <p className="pt-2 text-muted">Cross-promotions can provide an opportunity to make use of assets you already have,
                                                saving you money.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-plane text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Cooperate with influencers.</h4>
                                            <p className="pt-2 text-muted">So what is an influencer? A person or group that has the capability to influence the
                                                behaviour or opinions of others.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-arc text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Combined Advertising.</h4>
                                            <p className="pt-2 text-muted">Many Fast Food Restaurant Chains often indulge in joint advertising with beverages
                                                companies. You can do this by partnering with the company where marketing to your
                                                target audience is mutually beneficial.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-tools text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Co-produce a newsletter.</h4>
                                            <p className="pt-2 text-muted">You and a partner can co-produce a newsletter that ties your brands together in an organic way.
                                                Co-production will keep expenses low while expanding the types of expertise you can offer your
                                                clients.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                            <Col lg="4" className="margin-t-20">
                                <div className="services-box">
                                    <div className="media">
                                        <i className="pe-7s-timer text-custom"></i>
                                        <div className="media-body ml-4">
                                            <h4>Cross-promote your brands on social media</h4>
                                            <p className="pt-2 text-muted">If both you and your partner have an established social media presence, an opportunity
                                                is calling. By cross-promoting each other’s brands to particular followers, you can
                                                double your visibility and win new guests.</p>
                                        </div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}
export default Services;


