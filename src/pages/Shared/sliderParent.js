import React,{Component} from 'react'
import Slider from "./slider";
import './slider.css'

import img1 from '../../images/associates/apple.jpeg';
import {Card, CardColumns, Col} from "reactstrap";
import img2 from "../../images/clients/2.png";


export  default class SliderParent extends React.Component {
    render() {
        let data = [
            {name:"Apple",profession:"fruit",img:img1},
            {name:"Apple2",profession:"fruit",img:img1},
            {name:"Apple3",profession:"fruit",img:img1},
            {name:"Apple4",profession:"fruit",img:img1},
            {name:"Apple5",profession:"fruit",img:img1},
            {name:"Apple",profession:"fruit",img:img1},
            {name:"Apple2",profession:"fruit",img:img1},
            {name:"Apple3",profession:"fruit",img:img1},
            {name:"Apple4",profession:"fruit",img:img1},
            {name:"Apple5",profession:"fruit",img:img1},
            {name:"Apple",profession:"fruit",img:img1},
            {name:"Apple2",profession:"fruit",img:img1},
            {name:"Apple3",profession:"fruit",img:img1},
            {name:"Apple4",profession:"fruit",img:img1},
            {name:"Apple5",profession:"fruit",img:img1},
        ];
        return (
            <div className="parent">
                <Slider>
                    {data.map(value => {
                        return (
                            <div key={value.name} className="child">
                                <Card>
                                    <div>{value.name}</div>
                                    <div className="client-images my-4 my-md-0">
                                        <img src={value.img} alt="logo-img" width={150} height={150} />
                                    </div>
                                </Card>
                            </div>
                        );
                    })}
                </Slider>
            </div>
        );
    }
}