
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class WebsiteDescription extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        
        return (
            <React.Fragment>
                <section className="section bg-web-desc">
                    <div className="bg-overlay"></div>
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="text-white">WHAT WE DON'T?</h2>
                                <p className="padding-t-15 home-desc mx-auto">We don’t offer you a ‘one size fits all’ solution. We’ll look at your
                                    brand, understand your audience and create a tailor-made plan to
                                    achieve the results. By understanding who you are, what you want to
                                    convey and what is your USP, we rationalize and creatively develop a
                                    strategy that connects, engages and enhances your brand on digital
                                    media platforms, organically!</p>
                                {/*<Link to="#" onClick={evt => {  evt.preventDefault(); } } className="btn btn-bg-white margin-t-30 waves-effect waves-light">Know what we offer!</Link>*/}
                            </div>
                        </div>
                    </div>
                </section>
            </React.Fragment >
        );
    }
}

export default WebsiteDescription;


