import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';


import img1 from '../../images/clients/audi.png';
import img2 from '../../images/clients/2.png';
import img3 from '../../images/clients/3.png';
import img4 from '../../images/clients/client1.jpg';
import img5 from '../../images/clients/Assetsoft.jpg';
import img6 from '../../images/clients/congizant.png';
import img7 from '../../images/clients/Vaarivana.png';
import img8 from '../../images/clients/parandeSpaces.jpg';
import img9 from '../../images/clients/sahyadri.png';

class Client extends Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {

        return (
            <React.Fragment>
                <section style={{textAlign:'center' }} id={"clients"} className="section-sm bg-light">
                    <div className="container">
                        <Row style={{textAlign:'center' ,paddingBottom:60}}>
                            <Col lg="8" className="offset-lg-2">
                                <h1 className="section-title text-center">Our Clients</h1>


                            </Col>
                        </Row>
                        <Row>
                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img2} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>

                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img4} alt="logo-img" width={150} height={150} />
                                    </div>
                            </Col>

                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img3} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>
                        </Row>

                        <Row style={{paddingTop:60}}>
                            <Col md="4">
                                <div className="client-images my-3 my-md-0">
                                    <img src={img5} alt="logo-img" width={150} height={150}  />
                                </div>
                            </Col>

                            <Col md="4">
                                <div className="client-images my-3 my-md-0">
                                    <img src={img6} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>

                            <Col md="4">
                                <div className="client-images my-3 my-md-0" style={{textColor:'white'}}>
                                    <img src={img7} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>

                        </Row>

                        <Row style={{paddingTop:60}}>
                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img9} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>

                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img8} alt="logo-img" width={150} height={150} />
                                </div>
                            </Col>
                            <Col md="4">
                                <div className="client-images my-4 my-md-0">
                                    <img src={img1} alt="logo-img" width={150} height={150}  />
                                </div>
                            </Col>
                        </Row>
                    </div>
                </section>
            </React.Fragment >
        );
    }
}

export default Client;


